<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('user', 'Api\UserController@getUser');
    Route::post('logout', 'Api\LoginController@logout');
    Route::patch('updateProfile', 'Api\UserController@update');
    Route::post('uploadAvatar', 'Api\UserController@uploadAvatar');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('register', 'Api\RegisterController@register');
    Route::post('login', 'Api\LoginController@login');
    Route::post('refresh_token', 'Api\LoginController@refresh_token');
    Route::post('sendResetLink', 'Api\ForgotPasswordController@sendResetLink');
    Route::post('resetPassword', 'Api\ForgotPasswordController@resetPassword');
});
