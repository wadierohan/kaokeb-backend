# Kaokeb Test

Un test proposé par l'entreprise Kaokeb pour un éventuel embauche.

# Mission:

  - L’interface utilisateur en VueJS à utiliser est le repository opensource suivant : https://github.com/PanJiaChen/vue-element-admin
  - L’application web doit permettre à l’utilisateur de s’inscrire en demandant au minimum une adresse mail et un mot de passe.
  - L’utilisateur doit ensuite être capable de se connecter avec son email et son mot de passe.
  - Mettre en place une page profil lorsqu’on est connecté où il est possible de modifier son nom, prénom, mot de passe, mail et photo de profil.
  - L’utilisateur doit pouvoir recevoir un email de réinitialisation de son mot de passe en cas d’oubli. Mettre en place le système de mot de passe oublié. Le système de mail doit être fonctionnel.
  - L’utilisateur doit pouvoir se déconnecter en un seul clic dans la barre de menu.

### Tech

Les frameworks et les codes open-sources utilisés :

* [Laravel] - Laravel est un gratuit et open-source PHP web framework.
* [Vue.js] - Vue.js est un open-source framework JavaScript pour développer des interfaces utilisateur et des single-page applications.
* [PanJiaChen / vue-element-admin] - Vue-element-admin est un interface admin basé sur Vue et [Element UI]

### Installation

Cloner le repository, puis installer les dépendances puis lancer les commandes de configuration :

```sh
git clone https://bitbucket.org/wadierohan/kaokeb-backend.git
cd kaokeb-backend
composer install
php artisan app:install && php artisan app:dbconfig
```

Mais si vous aimez faire les étapes manuellement :

```sh
git clone https://bitbucket.org/wadierohan/kaokeb-backend.git
cd kaokeb-backend
composer install
cp .env.example .env # Copie .env.example vers .env
nano .env # Modifier le fichier .env -- PS : Vous pouvez ignorer App_Key
php artisan key:generate # ça va configurer le App_Key
php artisan storage:link # Créer un symlink vers resources/storage/app/public
php artisan config:cache # Un nettoyage de cache pour prendre en compte le nouveau fichier .env
php artisan migrate
php artisan passport:install
```
NB: Veuillez noter le résultat suivant:
```sh
Password grant client created successfully.
Client ID: 2
Client secret: vnvplrEe26yfo6VHgQ6bXw4OAQCXAPRD3g3qOJIP
Congratulation! You are ready to start.
```

Veuillez vérifier que le **Client ID** est bien configuré dans le fichier .env :
```env
PASSPORT_CLIENT_ID=2
```

### Running

Une fois la configuration faite, il ne reste plus qu'à lancer le serveur :
```sh
php artisan serv # Pour lancer le Built-in php server (Cette commande peut être ignorée si vous avez un web serveur)
```

Ensuite il faut installer le frontend par ici : https://bitbucket.org/wadierohan/kaokeb-frontend

**Un grand merci à l'équipe [Kaokeb](https://kaokeb.com) pour cet intéressant test**
**Wadie ELARRIM**

   [Laravel]: <https://laravel.com>
   [Vue.js]: <https://vuejs.org>
   [PanJiaChen / vue-element-admin]: <https://github.com/PanJiaChen/vue-element-admin>
   [Element UI]: <https://element.eleme.io>