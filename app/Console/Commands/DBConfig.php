<?php

namespace App\Console\Commands;

use PDOException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DBConfig extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:dbconfig';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'DB configuration and passport';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Check DB Connection
        $connected = $e = false;
        try {
            $connected = DB::connection()->getPdo();
        } catch (PDOException $e) {
        }
        
        if ($connected) {
            // Migrate
            $this->info('Migration...');
            $this->call('migrate');

            // Install Passport keys
            $this->info('Installing Passport...');
            $this->call('passport:install');

            $this->info('Congratulation! You are ready to start.');
        } else {
            $this->error('DB Connection Failed: '.$e->getMessage().' Check your DB Connection or your .env file then run : php artisan migrate');
        }
    }
}
