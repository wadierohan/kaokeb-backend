<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to install the app quickly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Because being original is great ;)
        $this->info("  ___                            ___                   ___      ");
        $this->info(" (   )                          (   )                 (   )     ");
        $this->info("  | |   ___     .---.    .--.    | |   ___     .--.    | |.-.   ");
        $this->info("  | |  (   )   / .-, \  /    \   | |  (   )   /    \   | /   \  ");
        $this->info("  | |  ' /    (__) ; | |  .-. ;  | |  ' /    |  .-. ;  |  .-. | ");
        $this->info("  | |,' /       .'`  | | |  | |  | |,' /     |  | | |  | |  | | ");
        $this->info("  | .  '.      / .'| | | |  | |  | .  '.     |  |/  |  | |  | | ");
        $this->info("  | | `. \    | /  | | | |  | |  | | `. \    |  ' _.'  | |  | | ");
        $this->info("  | |   \ \   ; |  ; | | '  | |  | |   \ \   |  .'.-.  | '  | | ");
        $this->info("  | |    \ .  ' `-'  | '  `-' /  | |    \ .  '  `-' /  ' `-' ;  ");
        $this->info(" (___ ) (___) `.__.'_.  `.__.'  (___ ) (___)  `.__.'    `.__.   ");
        $this->info("                                                                ");
        $this->info("                                                                ");

        // Check if env file exists, and if user is sure to override it.
        if (!File::exists(base_path('.env')) || (File::exists(base_path('.env')) && $this->confirm('Environment File already exists. Are you sure to override it?'))) {

            // Init all Variables
            ## APP INFO ##
            $this->info("############ APP INFO ############");
            $config['APP_NAME'] = $this->ask('What is your App name?');
            $config['APP_ENV'] = $this->choice('What is your App env?', [1 => 'local', 2 => 'production'], 'production');
            $APP_DEBUG = $this->confirm('Enable Debug?');
            $APP_DEBUG ? $config['APP_DEBUG'] = "true" : $config['APP_DEBUG'] = "false";
            $config['APP_URL'] = $this->ask('What is your Backend App url?', 'http://localhost');
            $config['FRONTEND_URL'] = $this->ask('What is your Frontend App url?', 'http://localhost:9527');

            ## DB INFO ##
            $this->info("############ DB INFO ############");
            $config['DB_CONNECTION'] = $this->ask('What is your DB Connection?', 'mysql');
            $config['DB_HOST'] = $this->ask('What is your DB Host?', 'localhost');
            $config['DB_PORT'] = $this->ask('What is your DB Port?', '3306');
            $config['DB_DATABASE'] = $this->ask('What is your DB Name?', 'admin');
            $config['DB_USERNAME'] = $this->ask('What is your DB Username?');
            $config['DB_PASSWORD'] = $this->secret('What is your DB Password?');

            ## MAIL INFO ##
            $this->info("############ MAIL INFO ############");
            $config['MAIL_DRIVER'] = $this->ask('What is your Mail Driver?', 'smtp');
            $config['MAIL_HOST'] = $this->ask('What is your Mail Host?', 'smtp.mailtrap.io');
            $config['MAIL_PORT'] = $this->ask('What is your Mail Port?', '2525');
            $config['MAIL_USERNAME'] = $this->ask('What is your Mail Username?');
            $config['MAIL_PASSWORD'] = $this->secret('What is your Mail Password?');
            $config['MAIL_ENCRYPTION'] = $this->ask('What is your Mail Encryption?');

            // Check if any config has a space
            foreach ($config as $key => $val) {
                if (strpos($val, ' ') !== false || strpos($key, 'PASSWORD') !== false) {
                    $config[$key] = '"'.$val.'"';
                }
            }

            // Create Env File
            $this->info('Creating .env file...');
            if (File::copy(base_path('.env.example'), base_path('.env'))) {
                file_put_contents(
                    base_path('.env'),
                    str_replace(
                        array_map(function ($val) {
                            return '{{'.$val.'}}';
                        }, array_keys($config)),
                        $config,
                        file_get_contents(base_path('.env'))
                    )
                );
            }

            // Generate new Key
            $this->info('Generating Key...');
            $this->call('key:generate');

            // Create Symbolic link to storage
            $this->info('Creating Symlink to storage...');
            $this->call('storage:link');

            // Clear Config
            $this->call('config:cache');

            $this->info('Next step => kindly run : php artisan app:dbconfig');
        }
    }
}
