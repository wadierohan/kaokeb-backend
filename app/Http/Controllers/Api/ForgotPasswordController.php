<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use App\PasswordReset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    // Request Password Reset Link
    public function sendResetLink(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);
        
        $email = $request->email;

        if (!User::where('email', $email)->exists()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Email address not found'
            ], 404);
        }

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse()
                    : $this->sendResetLinkFailedResponse($response);
    }

    // Change Password
    public function resetPassword(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'email' => ['required', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $passwordReset = PasswordReset::where([
            ['email', $request->email]
        ])->first();

        if (!$passwordReset || !Hash::check($request->token, $passwordReset->token)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Token or Email is invalid.'
            ], 404);
        }

        if (Carbon::parse($passwordReset->created_at)->addMinutes(config('auth.passwords.users.expire'))->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'status' => 'warning',
                'message' => 'Token Expired, please request a new one.'
            ], 404);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json([
                'status' => 'error',
                'message' => 'User doesn\'t exists.'
            ], 404);
        }

        $user->update(['password' => Hash::make($request->password)]);
        $passwordReset->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Your password has been updated.'
        ], 200);
    }

    // If Password Reset Link is Sent
    protected function sendResetLinkResponse()
    {
        return response()->json([
            'status' => 'success',
            'message' => 'Reset password link sent'
        ], 200);
    }

    // If Password Reset Link isn't Sent
    protected function sendResetLinkFailedResponse($response)
    {
        return response()->json([
            'status' => 'error',
            'message' => $response
        ], 500);
    }

    private function broker()
    {
        return Password::broker();
    }
}
