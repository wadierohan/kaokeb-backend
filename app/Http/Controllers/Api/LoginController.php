<?php

namespace App\Http\Controllers\Api;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\BadResponseException;

class LoginController extends Controller
{
    private $client;
    
    public function __construct()
    {
        $this->client = DB::table('oauth_clients')->where('id', config('services.passport.client_id'))->first();
    }

    public function login(Request $request)
    {
        $http = new Client();

        try {
            $response = $http->post(route('passport.token'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $this->client->id,
                    'client_secret' => $this->client->secret,
                    'username' => $request->username,
                    'password' => $request->password
                ]
            ]);
            return $response->getBody();
        } catch (BadResponseException $e) {
            if ($e->getCode() === 400) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Invalid request'
                ], $e->getCode());
            } elseif ($e->getCode() === 401) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Your credentials are incorrects'
                ], $e->getCode());
            }
            return response()->json([
                'status' => 'error',
                'message' => 'Something\'s wrong'
            ], $e->getCode());
        }
    }

    public function refresh_token(Request $request)
    {
        $http = new Client();

        try {
            $response = $http->post(route('passport.token'), [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'client_id' => $this->client->id,
                    'client_secret' => $this->client->secret,
                    'refresh_token' => $request->refresh_token
                ]
            ]);
            return $response->getBody();
        } catch (BadResponseException $e) {
            if ($e->getCode() === 400) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Invalid request'
                ], $e->getCode());
            } elseif ($e->getCode() === 401) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Please double check your configuration'
                ], $e->getCode());
            }
            return response()->json([
                'status' => 'error',
                'message' => 'Something\'s wrong'
            ], $e->getCode());
        }
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'status' => 'success',
            'message' => 'Cya later'
        ], 200);
    }
}
