<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function getUser(Request $request)
    {
        $user = $request->user();
        return response()->json($user, 200);
    }

    public function update(Request $request)
    {
        $user = $request->user();

        $validData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
            'old_password' => ['required_with:password'],
            'password' => ['nullable', 'min:8', 'confirmed'],
        ]);

        if ($request->password && !Hash::check($request->old_password, $user->password)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Wrong password'
            ], 401);
        }

        unset($validData['old_password']);
        if ($request->password) {
            $validData['password'] = Hash::make($request->password);
        } else {
            unset($validData['password']);
        }

        
        $user->update($validData);

        return response()->json([
            'status' => 'success',
            'message' => 'Profile Updated.',
            'data' => $user
        ], 200);
    }

    public function uploadAvatar(Request $request)
    {
        $request->validate([
            'avatar' => ['required', 'image', 'max:5120'],
        ]);

        $path = $request->avatar->store('avatars', 'public');
        $user = $request->user();
        $user->update(['avatar' => asset('storage/'.$path)]);

        return response()->json([
            'status' => 'success',
            'message' => 'Avatar Updated.',
            'avatar' => $user->avatar
        ], 200);
    }
}
